# Safelisting Gradle Repositories for Android Apps

If your top-level `build.gradle` file uses `include...()` functions (like
`includeModule()`) for all the repositories, your dependency resolution
works purely on a safelist basis. Artifacts will only be pulled from the
locations that you cite, and no unexpected artifacts will be included in your
builds.

This project demonstrates what that looks like, using a small sample app based
on one from [*Exploring Android*](https://commonsware.com/AndExplore).
See [this blog post](https://commonsware.com/blog/2021/02/20/using-repository-safelist-gradle.html)
and [the top-level `build.gradle` file](https://gitlab.com/commonsguy/gradlesafelist/-/blob/main/build.gradle)
for the results.

The contents of this repository are:

Copyright &copy; 2021 CommonsWare, LLC

The contents are licensed under the Apache Software License 2.0.
